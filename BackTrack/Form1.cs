﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackTrack
{
    public partial class Form1 : Form
    {
        bool[] visited = new bool[100];
        Tsequence Current = new Tsequence();    
        Tsequence result = new Tsequence();     
        public Form1()
        {
            InitializeComponent();
        }

        private void открытьФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog o = new OpenFileDialog();
            o.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (o.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.LoadFile(o.FileName, RichTextBoxStreamType.PlainText);
                richTextBox2.Clear();
            }
        }

        private void очиститьНаборToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox2.Clear();
        }


        private void SearchMax(int glub, char ch)
        {
            string str;
            for (int i = 0; i < richTextBox1.Lines.Length - 1; i++)     
            {
                str = richTextBox1.Lines[i];                            
                if (visited[i] == false)                                
                {
                    if (ch == '#' || ch == str[0])                      
                    {
                        visited[i] = true;                              
                        Current.arr[glub] = i;                          
                        Current.len = glub;                             
                        SearchMax(glub + 1, str[str.Length-1]);         
                        Current.len = glub - 1;                         
                        visited[i] = false;                             
                    }
                }
                if (Current.len > result.len)                           
                {
                    for (int j = 0; j < glub; j++)                      
                    {
                        result.arr[j] = Current.arr[j];                 
                    }
                    result.len = Current.len;                           
                }
            }
        }
        private void выполнитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox2.Clear();
            for (int i = 0; i < 100; i++)
            {
                visited[i] = false;                                     
            }
            Current.len = 0;                                            
            result.len = 0;
            SearchMax(0, '#');                                          
            for (int i = 0; i <= result.len; i++)                           
            {
                richTextBox2.AppendText(richTextBox1.Lines[result.arr[i]]);     
                richTextBox2.AppendText("\n");                                  
            }
        }

        private void сохранитьРезультатToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            if (save.ShowDialog() == DialogResult.OK)
            {
                richTextBox2.SaveFile(save.FileName, RichTextBoxStreamType.PlainText);
                MessageBox.Show("Успешно сохранено", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
